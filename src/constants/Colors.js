export default {
    primary: "#2962ff",
    secondary: "#B0E0E6",
    accent: "#d6d6c2",
    normalText: "#ffffff",

};