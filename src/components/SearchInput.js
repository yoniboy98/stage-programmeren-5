import React from 'react'
import {StyleSheet, TextInput,View, Text} from 'react-native';
import {Left} from 'native-base'
import {Ionicons} from '@expo/vector-icons';
import Colors from '../constants/Colors'
const SearchInput = props =>{
    return(
        <View style={styles.searchSection}>
            <Ionicons name='md-search' size={50} color='black' style={styles.searchIcon}/>
        <TextInput {...props}
                   style={{ ...styles.input, ...props.style}}
                   placeholder='Zoek hier uw product'
                   placeholderTextColor={Colors.normalText}
        >
        </TextInput>
        </View>
    )
}

const styles = StyleSheet.create({
    searchSection: {
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: 'transparent',
        width: '90%',
        height: 50,
        marginTop: 10,
        borderColor: 'black',
        borderWidth: 1,
        borderRadius: 5
    },
    input: {
            flex: 1,
            paddingTop: 10,
            paddingRight: 10,
            paddingBottom: 10,
            paddingLeft: 15,
            backgroundColor: 'transparent',
            color: 'black',
        borderColor: 'black',
        borderWidth: 0.5,
        borderRadius: 5,
        fontSize: 15

    },
    searchIcon:{
        padding: 10
    }
})
export default SearchInput;