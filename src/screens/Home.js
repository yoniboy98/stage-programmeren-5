import React, {useState} from 'react';
import {StyleSheet, View, Text, Picker, TouchableOpacity, ScrollView} from 'react-native';
import {Card, CardItem, Title, Content} from "native-base";
import SearchInput from '../components/SearchInput'
import Colors from "../constants/Colors";

function Home(props) {
    const [selectedValue, setSelectedValue] = useState("");
    const {navigation} = props;
    return (
        <View style={styles.container}>
            <Text style={styles.text}>Onze producten</Text>
            <SearchInput
                style={styles.input}
            />
            <View style={styles.picker}>
                <Picker
                    style={{height: 30, width: "100%"}}
                    color='white'
                    selectedValue={selectedValue}
                    onValueChange={(itemValue, itemIndex) => setSelectedValue(itemValue)}
                >
                    <Picker.Item label="-- Sorteer op --" value=""></Picker.Item>
                    <Picker.Item label="Sorteren op prijs" value="Sorteren op prijs"></Picker.Item>
                    <Picker.Item label="Sorteren op categorie" value="Sorteren op categorie"></Picker.Item>
                    <Picker.Item label="Sorteren op prijs" value="Sorteren op prijs"></Picker.Item>
                    <Picker.Item label="Sorteren op categorie" value="Sorteren op categorie"></Picker.Item>
                </Picker>
            </View>
            <ScrollView contentContainerStyle={styles.cardContainer}>
                <Card>
                    <CardItem header>
                        <Title style={styles.itemTitle}> Product Title </Title>
                    </CardItem>

                </Card>
            </ScrollView>
            <TouchableOpacity
                style={styles.buttonContainer}
                onPress={() => navigation.navigate('Orders')}>
                <Text style={{color: 'white'}}>Orders</Text>
            </TouchableOpacity>

        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: Colors.accent
    },
    cardContainer: {
        backgroundColor: Colors.accent,
        width: '90%',
        justifyContent:'center'
    },
    text: {
        color: '#ffffff',
        fontSize: 24,
        padding: 10,
        borderWidth: 2,
        borderColor: 'black',
        borderRadius: 5,
        marginTop: 15,
        backgroundColor: Colors.primary,
        width: '90%',
        textAlign: 'center'
    },
    itemTitle: {
        color: 'black'
    },
    buttonContainer: {
        backgroundColor: '#222',
        borderRadius: 5,
        padding: 10,
        margin: 20
    },
    buttonText: {
        fontSize: 20,
        color: '#fff'
    },
    input: {
        width: '90%'
    },
    picker: {
        alignItems: 'center',
        borderColor: 'black',
        borderWidth: 1,
        borderRadius: 5,
        width: '90%',
        backgroundColor: 'transparent',
        opacity: 0.8
    },
    pickerItem: {
        textAlign: 'center',
        alignItems: 'center',
        color: 'white'
    }
})

export default Home
