import React from 'react'
import { StyleSheet, View, Text, TouchableOpacity } from 'react-native'
import OrdersCards from '../components/OrdersCards';

function Orders(props) {
    const { navigation } = props
    return (
        <View style={styles.container}>
            <Text style={styles.text}>Orders Screen</Text>
            <TouchableOpacity>
          <OrdersCards style={styles.container}>
          </OrdersCards>
            </TouchableOpacity>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: '#ebebeb'
    },
    text: {
        color: '#101010',
        fontSize: 24,
        fontWeight: 'bold',

    },
    buttonContainer: {
        backgroundColor: '#222',
        borderRadius: 5,
        padding: 10,
        margin: 20
    },
    buttonText: {
        fontSize: 20,
        color: '#fff'
    }
})

export default Orders;
